[
    [
        components.Position(100, 100),
        components.Renderable("game/assets/rock.png"),
        components.Hitbox((32, 32)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(400, 100),
        components.Renderable("game/assets/rock.png"),
        components.Hitbox((32, 32)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(250, 400),
        components.Renderable("game/assets/rock.png"),
        components.Hitbox((32, 32)),
        components.Layer(z=0),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, 0),
        components.Renderable("game/assets/background.png"),
        components.Layer(z=-2)
    ],
    [
        components.Position(0, 0),
        components.Renderable("game/assets/walls.png"),
        components.Layer(z=-1)
    ],
    [
        components.Position(0, 0),
        components.Hitbox((32, HEIGHT)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, 0),
        components.Hitbox((WIDTH, 32)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(0, HEIGHT - 32),
        components.Hitbox((WIDTH, 32)),
        components.Type(components.EntityType.Object)
    ],
    [
        components.Position(WIDTH - 32, 0),
        components.Hitbox((32, HEIGHT)),
        components.Type(components.EntityType.Object)
    ]
]