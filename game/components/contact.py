from .. import ecs


# component attached to an entity when there is a contact between
# the moving one and the other entity, if both of them have
# Movement and Position components

class Contact(ecs.Component):
    def __init__(self, entity):
        self.entity = entity