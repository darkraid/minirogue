from .. import ecs


class Damage(ecs.Component):
    def __init__(self, quantity, from_: ecs.Entity):
        self.quantity = quantity
        self.from_ = from_