from .. import ecs
from .callback import Callback


class Door(ecs.Component):
    def __init__(self, dest: tuple, callback: Callback, active: bool):
        self.dest = dest
        self.callback = callback
        self.active = active