from .. import ecs
from ..utils.Vec2D import Vec2D

class HealthBar(ecs.Component):

    def __init__(self, color, size = Vec2D(15,5)):
        self.size = size
        self.color = color

