from .. import ecs


# useful for entities with a Renderable component:
# this helps the rendering system to know in which
# order it should draw the entities.
# entities with a layer z=0 will be drawn before any others,
# Z should be a positive integer

class Layer(ecs.Component):
    def __init__(self, z=0):
        self.z = z