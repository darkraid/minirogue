from .. import ecs
from ..utils.Vec2D import Vec2D


class Position(ecs.Component, Vec2D):
    def __init__(self, x, y):
        super().__init__(x, y)
    
    @property
    def position(self):
        return (self.x, self.y)
    
    def move(self, vec: Vec2D):
        self.x += vec.x
        self.y += vec.y