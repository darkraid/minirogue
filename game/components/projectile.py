from .. import ecs
from ..utils.Vec2D import Vec2D
import pygame


class Projectile(ecs.Component):
    def __init__(self, origin: Vec2D, vec_dir: Vec2D, origin_hitbox: pygame.Rect):
        self.origin = origin
        self.vec_dir = vec_dir
        self.origin_hitbox = origin_hitbox
