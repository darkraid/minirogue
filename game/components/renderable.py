import pygame
from .. import ecs


# each entity with a Renderable component should have as well
# a Layer component, and a Position component, otherwise it
# won't be rendered

class Renderable(ecs.Component):
    def __init__(self, sprite_path, update=0.2):
        if isinstance(sprite_path, str):
            self._sprite = pygame.image.load(sprite_path).convert_alpha()
            self.sprites = [self._sprite]
        elif isinstance(sprite_path, pygame.Surface):
            self._sprite = sprite_path
            self.sprites = [self._sprite]
        elif isinstance(sprite_path, list):
            # assuming object is a pygame.Surface if it's not a string
            self.sprites = [pygame.image.load(path) if isinstance(path, str) else path for path in sprite_path]
            self._sprite = self.sprites[0]
        
        self.current = 0
        self.max_update = update
        self.update = update
    
    @property
    def sprite(self):
        return self.sprites[self.current]
    
    @property
    def width(self):
        return self.sprite.get_width()
    
    @property
    def height(self):
        return self.sprite.get_height()