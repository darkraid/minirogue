from .. import ecs
from enum import Enum


class EntityType(Enum):
    Ammo = 0
    ContactDamage = 1
    Player = 2
    Ennemy = 3
    Object = 4
    ContactDamagePermanent = 5


class Type(ecs.Component):
    def __init__(self, etype: EntityType):
        self.type = etype