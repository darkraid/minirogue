from .. import ecs
from enum import Enum


class WeaponsHolder(ecs.Component):
    def __init__(self, owner: ecs.Entity, *weapons):
        self.owner = owner
        self.weapons = list(weapons)


class WeaponType(Enum):
    Fireball = 0
    Plasma = 1


class Weapon(ecs.Component):
    def __init__(self, cooldown, wtype: WeaponType, ranged: bool, active=False):
        self.max_cooldown = cooldown
        self.cooldown = cooldown
        self.type = wtype
        self.ranged = ranged
        self.active = active