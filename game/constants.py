# window size in pixels
WIDTH = 600
HEIGHT = 600

# framerate limit
FPS_LIMIT = 120

DEBUG = True

# entities speeds
# player
SPEED = 750  # pixels/second
# ennemies
ENNEMIES_SPEED = 350
# ranged weapons
AMMO_SPEED = 850
AMMO_SPEED_FIREBALL = 750
AMMO_SPEED_PLASMA = 850

# damages
DMG_FIREBALL = 10
DMG_PLASMA = 15
DMG_SWORD = 12