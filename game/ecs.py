class Entity:
    UID = 0

    def __init__(self):
        self._id = Entity.UID
        # auto increment entity unique id
        Entity.UID += 1
    
    def __repr__(self):
        return f"{type(self)} ({self._id})"

    def __eq__(self, other):
        return self._id == other._id
    
    def __hash__(self):
        return self._id


class EntityManager:
    def __init__(self):
        self._database = {}  # component type => { entity => component instance }
        self._entities = {}  # entity => component type
    
    def __len__(self):
        return len(self._entities)
    
    def add(self, entity, component_instance):
        # register entity
        if entity not in self._entities:
            self._entities[entity] = []

        # register component for entity
        ctype = type(component_instance)
        if ctype not in self._database:
            self._database[ctype] = {}
        self._database[ctype][entity] = component_instance
        self._entities[entity].append(ctype)

        return self
    
    def hasEntity(self, entity):
        return entity in self._entities
    
    def getComponentTypes(self, entity):
        # return all the components of a given entity
        try:
            return self._entities[entity]
        except KeyError:
            return []
    
    def get(self, entity, ctype):
        # return a single component instance from a given entity and a component type
        try:
            return self._database[ctype][entity]
        except KeyError:
            return None
    
    def removeComponent(self, entity, ctype):
        # try to remove a component from a given entity
        try:
            del self._database[ctype][entity]
            if self._database[ctype] == {}:
                del self._database[ctype]
            
            self._entities[entity].remove(ctype)
            if self._entities[entity] == []:
                del self._entities[entity]
        except KeyError:
            pass
        
        return self
    
    def removeEntity(self, entity):
        # remove the entity, and all its components
        if entity in self._entities:
            del self._entities[entity]

            to_remove = []
            for ctype, data in self._database.items():
                if entity in data:
                    to_remove.append(ctype)
            
            for ctype in to_remove[::-1]:
                del self._database[ctype][entity]
                if self._database[ctype] == {}:
                    del self._database[ctype]
        
        return self
    
    def iter(self, ctype):
        # retrieve components based on their type
        if ctype in self._database:
            for entity, component in self._database[ctype].items():
                yield entity, component
    
    def iterAll(self, ctypes: list):
        if len(ctypes) == 1:
            for e, c in self.iter(ctypes[0]):
                yield e, c
        else:
            for entity, component_types in self._entities.items():
                if all(c in component_types for c in ctypes):
                    components = [self._database[ctype][entity] for ctype in ctypes]
                    yield entity, components


class Component:
    pass


class System:
    def __init__(self, entity_manager, *component_types):
        self.entity_manager = entity_manager
        self._component_types = list(component_types)
    
    def __iter__(self):
        for entity, components in self.entity_manager.iterAll(self._component_types):
            yield entity, components
    
    def update(self, dt):
        raise NotImplementedError