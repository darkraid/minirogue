import pygame
from . import systems
from . import components
from . import ecs
from .constants import *
import math
from .utils.Vec2D import Vec2D
from random import randint
from .levelManager import LevelManager


class Game:
    def __init__(self, screen):
        self.screen = screen
        self._systems = []
        self.running = True
        self._clock = pygame.time.Clock()

        # summoning the Level Manager
        self._levelmgr = LevelManager(self.screen)
        self._levelmgr.load()
        self._levelmgr.init()  # must be called after levelmgr.load()
        self._player = self._levelmgr.generatePlayer()
        self._levelmgr.generateCurrentRoom(0, self._player)

        self._move = Vec2D()
    
    def update(self, dt: float):
        for system in self._levelmgr.systems:
            system.update(dt)
    
    def dispatchEvent(self, event):
        if event.type == pygame.QUIT:
            self.running = False
        
        # handle player movement, separated from the standard systems
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self._move.y = -SPEED
            if event.key == pygame.K_DOWN:
                self._move.y = SPEED
            if event.key == pygame.K_LEFT:
                self._move.x = -SPEED
            if event.key == pygame.K_RIGHT:
                self._move.x = SPEED
        
        # summon projectile
        if event.type == pygame.MOUSEBUTTONUP:
            player_hitbox = self._levelmgr.entity_manager.get(self._player, components.Hitbox)
            mx, my = event.pos
            dx, dy = mx - player_hitbox.center.x, my - player_hitbox.center.y
            n = (dx ** 2 + dy ** 2) ** 0.5
            vec_dir = Vec2D(dx / n, dy / n)
            
            self._levelmgr.entity_manager.add(self._player, components.Projectile(player_hitbox.center, vec_dir, player_hitbox.rectangle))
    
    def run(self):
        while self.running:
            # dt is in milliseconds
            dt = self._clock.tick(FPS_LIMIT)
            
            e_count = len(self._levelmgr.entity_manager)
            pygame.display.set_caption(f"MiniPolyRogue - FPS: {1.0 / dt * 1000:.1f} - Entities : {e_count}")

            self.dispatchEvent(pygame.event.poll())
            movement = components.Movement(self._move)
            self._move = Vec2D()
            self._levelmgr.entity_manager.add(self._player, movement)

            # the delta time given to the update method must be in seconds
            self.update(dt / 1000)

            pygame.display.flip()