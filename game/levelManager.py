from . import systems
from . import components
from . import ecs
from .constants import *
from random import randint, choice
from glob import glob
from enum import Enum
from .utils.Vec2D import Vec2D


def print_r(L: list):
    for y, line in enumerate(L):
        for x, case in enumerate(line):
            if case is None:
                print('☓', end='')
            elif case.room_type == RoomType.Start:
                print('◈', end='')
            elif case.room_type == RoomType.End:
                print('◉', end='')
            elif case.room_type == RoomType.Unexplored:
                print('▢', end='')
            elif case.room_type == RoomType.Explored:
                print('▩', end='')
            elif case.room_type == RoomType.EndPath:
                print('▼', end='')
        print(end='\n')
    print('', flush=True)


class AntState(Enum):
    Alive = 0
    Dead = 1


class Ant:
    def __init__(self, state: AntState, pos: tuple):
        self.state = state
        self._pos = pos
    
    @property
    def x(self):
        return self._pos[0]
    
    @x.setter
    def x(self, value):
        self._pos = (value, self.y)
    
    @property
    def y(self):
        return self._pos[1]
    
    @y.setter
    def y(self, value):
        self._pos = (self.x, value)


class RoomType(Enum):
    Start = 0
    End = 1
    Unexplored = 2
    Explored = 3
    EndPath = 4


class Room:
    UID = 0

    def __init__(self, room_type: RoomType, id_: int):
        self.room_type = room_type
        self.id = id_
        self.content = None

        self._uid = Room.UID
        Room.UID += 1
    
    def __hash__(self):
        return self._uid


sign = lambda x: -1 if x < 0 else +1


class LevelManager:
    def __init__(self, screen):
        self.screen = screen

        # data will be a NxN matrix, with N between 3 and 9
        # None means no room, other than None is a room
        self._data = []  # y => [x => Room]
        self._size = 0
        self._rooms_available = list(glob("game/assets/rooms/*.txt"))

        self._start_room = None
        self._end_room = None
        self._current_room = None
        self._entities = {}  # room position (tuple) => entities manager
        self._systems = {}  # room position (tuple) => [systems]
        self._players = []
        self._has_put_treasure = False
    
    @property
    def entity_manager(self):
        return self._entities[self._current_room]
    
    @property
    def systems(self):
        for s in self._systems[self._current_room]:
            yield s
    
    def _loadRoom(self, idx):
        with open(self._rooms_available[idx]) as file:
            try:
                return eval(file.read())
            except SyntaxError:
                print(f"The level {idx} is ill-formed")
                return []
    
    def _chooseRoomId(self):
        return randint(0, len(self._rooms_available) - 1)
    
    def _neighbours(self, x, y):
        # return list of position of rooms next to the position given
        out = []
        if x > 0 and self._data[y][x - 1]:
            out.append((x - 1, y))
        if y > 0 and self._data[y - 1][x]:
            out.append((x, y - 1))
        if x + 1 < self._size and self._data[y][x + 1]:
            out.append((x + 1, y))
        if y + 1 < self._size and self._data[y + 1][x]:
            out.append((x, y + 1))
        return out
    
    def load(self):
        self._size = randint(3, 9)
        # choosing the start and end point
        axis = choice(('x', 'y'))
        if axis == 'x':
            self._start_room = (randint(0, self._size - 1), 0)
            self._end_room = (randint(0, self._size - 1), self._size - 1)
        elif axis == 'y':
            self._start_room = (0, randint(0, self._size - 1))
            self._end_room = (self._size - 1, randint(0, self._size - 1))
        
        # creating the scheme
        for y in range(self._size):
            line = []
            for x in range(self._size):
                line.append(None)
            self._data.append(line)

        # putting the start and end rooms
        self._data[self._start_room[1]][self._start_room[0]] = Room(RoomType.Unexplored, self._chooseRoomId())
        self._data[self._end_room[1]][self._end_room[0]] = Room(RoomType.Unexplored, self._chooseRoomId())

        # minimum number of rooms to put
        room_number = 2 * self._size
        while room_number != 0:
            x = randint(0, self._size - 1)
            y = randint(0, self._size - 1)

            # empty space to put a room
            if self._data[y][x] == None:
                self._data[y][x] = Room(RoomType.Unexplored, self._chooseRoomId())
                room_number -= 1

        # creating base ant
        ants = []
        ants.append(Ant(AntState.Alive, (self._start_room[0], self._start_room[1])))

        def unexplored_rooms():
            # return number of unexplored rooms
            nonlocal self, ants
            count = 0
            for y, line in enumerate(self._data):
                for x, room in enumerate(line):
                    if room and room.room_type == RoomType.Unexplored:
                        count += 1
            return count
        
        def get_end_path():
            # return a list of the positions of the end of paths rooms
            nonlocal self, ants
            positions = []
            for y, line in enumerate(self._data):
                for x, room in enumerate(line):
                    if room and room.room_type == RoomType.EndPath:
                        positions.append((x, y))
            return positions

        def is_there_an_ant(x, y):
            nonlocal self, ants
            for ant in ants:
                if ant.state == AntState.Alive and ant.x == x and ant.y == y:
                    return True
            return False
        
        def get_neighbours(x, y):
            # return list of position of unexplored rooms next to the position given
            nonlocal self, ants
            out = []
            if x > 0 and self._data[y][x - 1] and self._data[y][x - 1].room_type == RoomType.Unexplored:
                out.append((x - 1, y))
            if y > 0 and self._data[y - 1][x] and self._data[y - 1][x].room_type == RoomType.Unexplored:
                out.append((x, y - 1))
            if x + 1 < self._size and self._data[y][x + 1] and self._data[y][x + 1].room_type == RoomType.Unexplored:
                out.append((x + 1, y))
            if y + 1 < self._size and self._data[y + 1][x] and self._data[y + 1][x].room_type == RoomType.Unexplored:
                out.append((x, y + 1))
            return out
        
        def find_nearest_unexplored_room_from_end_points():
            # return tuple (unexplored room pos, endpoint pos)
            nonlocal self, ants
            rooms = []  # list of (endpoint pos, unexplored room pos, x^2+y^2)
            endpoints = get_end_path()

            def find_nearest_unexplored(x, y):
                # returns list of positions of the nearest unexplored rooms from (x, y)
                nonlocal self, ants
                distance = 1

                output = []

                while True:
                    left = x - distance
                    right = x + distance
                    top = y - distance
                    bottom = y + distance

                    if left >= 0:
                        for ty in range(max(0, top), min(self._size, bottom + 1)):
                            room = self._data[ty][left]
                            if room and room.room_type == RoomType.Unexplored:
                                output.append((left, ty))
                    if right < self._size:
                        for ty in range(max(0, top), min(self._size, bottom + 1)):
                            room = self._data[ty][right]
                            if room and room.room_type == RoomType.Unexplored:
                                output.append((right, ty))
                    if top >= 0:
                        for tx in range(max(0, left), min(self._size, right + 1)):
                            room = self._data[top][tx]
                            if room and room.room_type == RoomType.Unexplored:
                                output.append((tx, top))
                    if bottom < self._size:
                        for tx in range(max(0, left), min(self._size, right + 1)):
                            room = self._data[bottom][tx]
                            if room and room.room_type == RoomType.Unexplored:
                                output.append((tx, bottom))
                    
                    # once we found unexplored rooms, stop
                    if output:
                        break

                    distance += 1
                return output

            for e in endpoints:
                for unex in find_nearest_unexplored(*e):
                    rooms.append((
                        e, unex, (unex[0] - e[0]) ** 2 + (unex[1] - e[1]) ** 2
                    ))
            
            # sort rooms array to remove endpoints going to the same unexplored rooms
            # we will only keep the ones with the shorter distance
            unexplored_dict = {}  # unexplored room pos => list of (endpoint pos, x^2+y^2)
            for (endpoint, unexplored, squared_dist) in rooms:
                if unexplored not in unexplored_dict:
                    unexplored_dict[unexplored] = []
                unexplored_dict[unexplored].append((endpoint, squared_dist))
            temp = {}
            for k in unexplored_dict.keys():
                temp[k] = sorted(unexplored_dict[k], key=lambda x: x[1])
            # only keep the nearest endpoints
            for k in temp.keys():
                temp[k] = temp[k][0]
            # only keep the couple (unexplored, endpoint) with the shorter squared distance
            k, (end, _) = sorted(temp.items(), key=lambda x: x[1][1])[0]
            
            return (k, end)

        while True:
            unexplored = unexplored_rooms()
            if unexplored == 0:
                break

            to_spawn = []

            for i, ant in enumerate(ants):
                if ant.state == AntState.Alive:
                    # search for neighbours
                    neighbours = get_neighbours(ant.x, ant.y)

                    # mark current room as explored
                    room = self._data[ant.y][ant.x]
                    if room and room.room_type == RoomType.Unexplored:
                        self._data[ant.y][ant.x].room_type = RoomType.Explored

                    # if we don't have any neighbours, we are at a dead node
                    if len(neighbours) == 0:
                        rtype = self._data[ant.y][ant.x].room_type
                        self._data[ant.y][ant.x].room_type = RoomType.EndPath
                        
                        # find nearest room and do stuff (create rooms to go there)
                        if unexplored_rooms() == 0:
                            ant.state = AntState.Dead
                            break
                        
                        # mapping unexplored rooms position to endpoint position
                        u, ep = find_nearest_unexplored_room_from_end_points()

                        # mark current endpoint as an explored room
                        self._data[ep[1]][ep[0]].room_type = RoomType.Explored
                        # choose a direction to the unexplored room
                        # based on the components of the vec_dir
                        a, b = (u[0] - ep[0], u[1] - ep[1])
                        # if their absolute values are equal, choose an axis randomly
                        if abs(a) == abs(b):
                            axis = choice(('x', 'y'))
                            if axis == 'x':
                                self._data[ep[1]][ep[0] + sign(a)] = Room(RoomType.Unexplored, self._chooseRoomId())
                            elif axis == 'y':
                                self._data[ep[1] + sign(b)][ep[0]] = Room(RoomType.Unexplored, self._chooseRoomId())
                        elif abs(a) > abs(b):
                            self._data[ep[1]][ep[0] + sign(a)] = Room(RoomType.Unexplored, self._chooseRoomId())
                        else:
                            self._data[ep[1] + sign(b)][ep[0]] = Room(RoomType.Unexplored, self._chooseRoomId())

                    elif len(neighbours) == 1:
                        # if we have a single neighbour, continue and go to this one, if we can
                        if not is_there_an_ant(*neighbours[0]):
                            ant.x, ant.y = neighbours[0]
                        else:
                            ant.state = AntState.Dead
                    else:
                        # there are multiple neighbours, kill current ants, and spawn children
                        ant.state = AntState.Dead
                        for n in neighbours:
                            if not is_there_an_ant(*n):
                                # spawn new ant
                                to_spawn.append(Ant(AntState.Alive, n))
            
            # spawn ants if needed
            for ant in to_spawn:
                ants.append(ant)
            
            # remove dead ants
            for ant in ants:
                if ant.state == AntState.Dead:
                    ants.remove(ant)
        
        # kill all ants
        del ants

        # mark end room
        self._data[self._start_room[1]][self._start_room[0]].room_type = RoomType.Start
        self._data[self._end_room[1]][self._end_room[0]].room_type = RoomType.End
        
        # loading the rooms
        for y, line in enumerate(self._data):
            for x, room in enumerate(line):
                if room:
                    self._data[y][x].content = self._loadRoom(room.id)
    
    def generatePlayer(self):
        # must be called once we have a valid entity manager
        # return a "player" entity

        player = ecs.Entity()
        self._entities[self._current_room].add(player, components.Position(WIDTH // 2, HEIGHT // 2)) \
            .add(player, components.Health(24)) \
            .add(player, components.HealthBar((0, 255, 0), Vec2D(32, 5))) \
            .add(player, components.Renderable([
                "game/assets/player/player_0.png",
                "game/assets/player/player_1.png",
                "game/assets/player/player_2.png",
                ])) \
            .add(player, components.Hitbox((32, 32))) \
            .add(player, components.Layer(z=0)) \
            .add(player, components.Type(components.EntityType.Player)) \
            .add(player, components.WeaponsHolder(
                player,
                components.Weapon(0.0, components.WeaponType.Fireball, True, active=True),
                components.Weapon(0.0, components.WeaponType.Plasma, True, active=True)
                )) \
            .add(player, components.Score()) \
            .add(player, components.Bag())
        self._players.append(player)

        return self._players[-1]
    
    def init(self):
        # must be called once, before generating the first room, after loading the level
        self._current_room = self._start_room
        self._entities[self._start_room] = ecs.EntityManager()

        if DEBUG:
            print_r(self._data)
    
    def generateCurrentRoom(self, score, player: ecs.Entity, room=None):
        # must only contains objects and overworld modifications
        # the game will spawn ennemies based on the score of the player
        # random chance to put a treasure, one per level
        # must keep track of the entities created to deactivate them when the player is going to another level
        
        if room is None:
            self._current_room = self._start_room
        else:
            self._current_room = room
        
        print(self._current_room)
        
        if self._current_room not in self._systems:
            if self._current_room not in self._entities:
                self._entities[self._current_room] = ecs.EntityManager()
            self._systems[self._current_room] = []

            # create systems
            for stype in systems.__all__:
                self._systems[self._current_room].append(stype(self._entities[self._current_room], self.screen))
            
            # adding the components of the room if it hasn't been done yet
            for components_list in self._data[self._current_room[1]][self._current_room[0]].content:
                e = ecs.Entity()
                for c in components_list:
                    self._entities[self._current_room].add(e, c)
            
            def callback_door(room):
                # we should remove the player from the current room's entity manager
                # to put it in the new room
                nonlocal self
                player_components = []
                for ctype in self.entity_manager.getComponentTypes(player):
                    # we don't want to copy a callback
                    if ctype != components.Callback:
                        player_components.append(self.entity_manager.get(player, ctype))
                
                # leave the player in the room with only its callback component
                for ctype in player_components:
                    self.entity_manager.removeComponent(player, ctype)
                # reset the hitbox component of the player
                self.entity_manager.get(player, components.Hitbox).collide_groups = None

                ## -- FROM THERE, WE ARE USING A NEW ENTITY MANAGER --
                # generate the room and put a player in it
                self.generateCurrentRoom(self.entity_manager.get(player, components.Score).value, player, room)
                for c in player_components:
                    self.entity_manager.add(player, c)
                # overwrite position component
                self.entity_manager.add(player, components.Position(WIDTH // 2, HEIGHT // 2))
                self.entity_manager.get(player, components.Hitbox).collide_groups = None

            # generating the doors
            neighbours = self._neighbours(self._current_room[0], self._current_room[1])
            for n in neighbours:
                e = ecs.Entity()
                relpos = (n[0] - self._current_room[0], n[1] - self._current_room[1])
                x = (1 + relpos[0]) * WIDTH // 2
                y = (1 + relpos[1]) * HEIGHT // 2
                if x == 600:
                    x -= 50
                if y == 600:
                    y -= 50
                # we need to trick the lazy evaluation system, to force it to create a lexical scope now retaining
                # the copy of n given to f 3 lines below
                f = lambda dn: (lambda dt: callback_door(dn))
                self._entities[self._current_room].add(e, components.Position(x, y)) \
                    .add(e, components.Door(n, components.Callback(f((n[0], n[1]))), False)) \
                    .add(e, components.Hitbox((50, 50))) \
                    .add(e, components.Type(components.EntityType.Object))
                # TODO add door image (renderable, layer needed)
        
            # TODO remove, testing ; use score to generate the ennemies
            for i in range(0):
                e = ecs.Entity()
                x, y = WIDTH // 2, HEIGHT // 2
                rx, ry = randint(1, 10) / 10, randint(1, 10) / 10
                self._entities[self._current_room].add(e, components.Position(x, y)) \
                    .add(e, components.Hitbox((32, 32))) \
                    .add(e, components.Layer(z=2)) \
                    .add(e, components.Renderable("game/assets/player.png")) \
                    .add(e, components.Movement(Vec2D(rx, ry) * ENNEMIES_SPEED)) \
                    .add(e, components.Type(components.EntityType.Ennemy)) \
                    .add(e, components.Health(24)) \
                    .add(e, components.HealthBar((255, 0, 170), Vec2D(32, 5)))
        
        # TODO remove
        for e, d in self.entity_manager.iter(components.Door):
            print(e, d.dest)