import pygame
from .game import Game
from . import systems
from . import components
from . import ecs
from .constants import *
from .utils.Vec2D import Vec2D


class Menu:
    def __init__(self, screen):
        self.screen = screen
        self.running = True
        self._systems = []
        self._clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 50)
        self._entity_manager = ecs.EntityManager()

        self._playText = ecs.Entity()
        self._optionText = ecs.Entity()
        self._quitText = ecs.Entity()

        self._playHitbox = pygame.Rect((150, 0, 300, 50))
        self._optionHitbox = pygame.Rect((150, 60, 300, 50))
        self._quitHitbox = pygame.Rect((150, 120, 300, 50))

        self._playposition = components.Position(150, 0)
        self._optionposition = components.Position(150, 60)
        self._quitposition = components.Position(150, 120)

        self._buttonLayer = components.Layer(z=0)

        self._pygame_event = ecs.Entity()

    def dispatchEvent(self, event):
        if event.type == pygame.QUIT:
            self.running = False
        
        if event.type == pygame.MOUSEBUTTONDOWN:
            self._entity_manager.add(self._pygame_event, components.MouseClickEvent(Vec2D(*event.pos), False))

    def update(self, dt: float):
        for system in self._systems:
            system.update(dt)
    
    def _close(self):
        self.running = False

    def _prepare(self):
        for stype in (systems.DestructionSystem, systems.RenderingSystem, systems.EventSystem, systems.CallbackSystem):
            self._systems.append(stype(self._entity_manager, self.screen))

        self._entity_manager.add(self._playText, self._playposition) \
            .add(self._playText, self._buttonLayer) \
            .add(self._playText, components.Renderable(self.font.render("PLAY", True, pygame.Color('red')))) \
            .add(self._playText, components.OnMouseClickEvent(self._playHitbox, False, [
                components.Callback(lambda dt: Game(self.screen).run())
            ], permanent=True))

        self._entity_manager.add(self._optionText, self._optionposition) \
            .add(self._optionText, self._buttonLayer) \
            .add(self._optionText, components.Renderable(self.font.render("OPTIONS", True, pygame.Color('red'))))

        self._entity_manager.add(self._quitText, self._quitposition) \
            .add(self._quitText, self._buttonLayer) \
            .add(self._quitText, components.Renderable(self.font.render("QUIT", True, pygame.Color('red')))) \
            .add(self._quitText, components.OnMouseClickEvent(self._quitHitbox, False, [
                components.Callback(lambda dt: self._close())
            ], permanent=True))

    def run(self):
        self._prepare()

        while self.running:
            # dt is in milliseconds
            dt = self._clock.tick(FPS_LIMIT)
            
            if DEBUG:
                pygame.display.set_caption(f"MiniPolyRogue - FPS: {1.0 / dt * 1000:.1f}")

            self.dispatchEvent(pygame.event.poll())

            # the delta time given to the update method must be in seconds
            self.update(dt / 1000)
            pygame.display.flip()


def start(screen):
    menu = Menu(screen)
    menu.run()