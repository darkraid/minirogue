from .destructionSystem import DestructionSystem
from .healthSystem import HealthSystem
from .renderingSystem import RenderingSystem
from .movementSystem import MovementSystem
from .contactSystem import ContactSystem
from .weaponSystem import WeaponSystem
from .timerSystem import TimerSystem
from .healthbarSystem import HealthbarSystem
from .callbackSystem import CallbackSystem
from .eventSystem import EventSystem
from .doorSystem import DoorSystem


# the destruction system should be the first one in __all__,
# because the Game scene is using this tuple to instanciate all
# the needed system, in the same order as they are listed below.
# since entities can gain a Destroyed component at runtime, they
# shouldn't be destroyed before another system can use the component
# if needed

__all__ = (
    DestructionSystem,
    HealthSystem,
    RenderingSystem,
    MovementSystem,
    ContactSystem,
    WeaponSystem,
    TimerSystem,
    HealthbarSystem,
    CallbackSystem,
    EventSystem,
    DoorSystem,
)