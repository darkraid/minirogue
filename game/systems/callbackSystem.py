from .. import ecs
from ..components.callback import Callback
from ..utils.Vec2D import Vec2D


# if there is a callback attached to an entity, execute it
# then remove the callback component

class CallbackSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Callback)

    def update(self, dt):
        to_remove = []

        for e, callback in self:
            to_remove.append(e)
            callback(dt)
        
        for e in to_remove:
            self.entity_manager.removeComponent(e, Callback)