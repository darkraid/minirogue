from .. import ecs
from ..components.contact import Contact
from ..components.destroyed import Destroyed
from ..components.types import Type, EntityType
from ..components.movement import Movement
from ..components.health import Health
from ..components.damage import Damage
from ..constants import DEBUG, AMMO_SPEED


class ContactSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Contact)
    
    def _is_damage_dealer(self, e):
        return self.entity_manager.get(e, Damage) is not None
    
    def _is_damage_taker(self, e):
        return self.entity_manager.get(e, Health) is not None

    def update(self, _):
        to_remove = []

        for entity, contact in self:
            to_remove.append(entity)
            
            entype = self.entity_manager.get(entity, Type)
            cetype = self.entity_manager.get(contact.entity, Type)
            # if there are no entity type, do nothing
            # we don't want to do something when an Ammo hits a wall for example
            if entype and cetype:
                # invert ammo movement if it gets hits by a contact damage
                if entype.type == EntityType.Ammo and cetype.type == EntityType.ContactDamage:
                    self.entity_manager.get(entity, Movement).vel.invert()
                elif entype.type == EntityType.ContactDamage  and cetype.type == EntityType.Ammo:
                    self.entity_manager.get(contact.entity, Movement).vel.invert()
                # if an ennmy hits a wall, invert its velocity
                elif entype.type == EntityType.Ennemy and cetype.type == EntityType.Object:
                    self.entity_manager.get(entity, Movement).vel.invert()
                elif entype.type == EntityType.Object and cetype.type == EntityType.Ennemy:
                    self.entity_manager.get(contact.entities, Movement).vel.invert()
                # if there is a damager taker and a damage dealer, add damages
                elif self._is_damage_dealer(entity) and self._is_damage_taker(contact.entity):
                    health = self.entity_manager.get(contact.entity, Health)
                    damage = self.entity_manager.get(entity, Damage)
                    if health and damage:
                        health.hp -= damage.quantity
                        health.last_modifier = damage.from_
                elif self._is_damage_taker(entity) and self._is_damage_dealer(contact.entity):
                    health = self.entity_manager.get(entity, Health)
                    damage = self.entity_manager.get(contact.entity, Damage)
                    if health and damage:
                        health.hp -= damage.quantity
                
                # remove ammo
                if entype.type == EntityType.Ammo:
                    self.entity_manager.add(entity, Destroyed())
                if cetype.type == EntityType.Ammo:
                    self.entity_manager.add(contact.entity, Destroyed())
                
                # remove contact damage, if it's not a permanent contact damage
                if entype.type == EntityType.ContactDamage:
                    self.entity_manager.add(entity, Destroyed())
                if cetype.type == EntityType.ContactDamage:
                    self.entity_manager.add(contact.entity, Destroyed())

        # remove Contact component from the entities
        for entity in to_remove:
            self.entity_manager.removeComponent(entity, Contact)