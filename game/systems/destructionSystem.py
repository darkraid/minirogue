from .. import ecs
from ..components.destroyed import Destroyed


class DestructionSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Destroyed)

    def update(self, _):
        entities = []
        for entity, _ in self:
            entities.append(entity)
        
        for e in entities[::-1]:
            self.entity_manager.removeEntity(e)