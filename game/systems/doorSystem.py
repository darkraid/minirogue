from .. import ecs
from ..components.door import Door
from ..components.types import Type, EntityType


# doors must be activated only if we killed all the ennemies in a room

class DoorSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, Door)
    
    def update(self, _):
        for e, type_ in self.entity_manager.iter(Type):
            if type_.type == EntityType.Ennemy:
                # quit the function to avoid activating the doors
                return

        for e, door in self:
            door.active = True