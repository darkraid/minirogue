from .. import ecs
from ..components.healthbar import HealthBar
from ..components.hitbox import Hitbox
from ..components.health import Health
from ..utils.Vec2D import Vec2D
import pygame


class HealthbarSystem(ecs.System):
    def __init__(self, entity_manager, screen, *args):
        super().__init__(entity_manager, Hitbox, Health, HealthBar)
        self.screen = screen

    def update(self, _):
        for e, (hitbox, health, healthbar) in self:
            width = healthbar.size.x * health.hp / health.max_hp
            pos = Vec2D(hitbox.rectangle.x, hitbox.rectangle.y - 10)
            pygame.draw.rect(self.screen, healthbar.color, (pos.x, pos.y, width, healthbar.size.y))