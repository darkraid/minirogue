import pygame
from .. import ecs
from ..components.movement import Movement
from ..components.position import Position
from ..components.hitbox import Hitbox
from ..components.destroyed import Destroyed
from ..components.contact import Contact
from ..components.renderable import Renderable
from ..components.types import Type, EntityType
from ..components.door import Door
from ..constants import WIDTH, HEIGHT, DEBUG
from copy import copy, deepcopy
from ..utils.Vec2D import Vec2D


# If an entity moved and appears to be outside the screen,
# it should be destroyed
# This system is also checking for collisions and contacts
# If there is a collision between an active door and a player,
# we should attach the callback of the door to the player

class MovementSystem(ecs.System):
    def __init__(self, entity_manager, screen, *args):
        super().__init__(entity_manager, Movement, Position, Hitbox)
        self._screen_rect = pygame.Rect(0, 0, WIDTH, HEIGHT)
        self.screen = screen

        self._groups = []
        self._groups_dict = {}  # group id => [entities...]
        self._slicing = 5
        # generating the group rects
        for x in range(self._slicing):
            for y in range(self._slicing):
                self._groups.append(pygame.Rect(
                    x * WIDTH // self._slicing, y * HEIGHT // self._slicing,
                    WIDTH // self._slicing, HEIGHT // self._slicing
                ))

    def update(self, dt):
        # update hitboxes for moving components
        for e, (position, hitbox) in self.entity_manager.iterAll([Position, Hitbox]):
            # update hitbox position
            renderable = self.entity_manager.get(e, Renderable)
            if renderable is None:
                hitbox.rectangle.x = position.x
                hitbox.rectangle.y = position.y
            else:
                hitbox.center = Vec2D(position.x + renderable.width / 2, position.y + renderable.height / 2)
            
            # add the collide groups in which the hitbox is
            if hitbox.collide_groups is None:
                hitbox.collide_groups = []
                for i, group in enumerate(self._groups):
                    if hitbox.rectangle.colliderect(group):
                        hitbox.collide_groups.append(i)
                        # add it in the group to entities mapping
                        if i not in self._groups_dict:
                            self._groups_dict[i] = []
                        self._groups_dict[i].append(e)
            
            if DEBUG:
                pygame.draw.rect(self.screen, (255, 0, 0), hitbox.rectangle, 2)

        for e, (movement, position, hitbox) in self:
            colliding_with = None

            # test if the new position will create a collision or not
            temp_hitbox = deepcopy(hitbox)
            temp_hitbox.move(movement.vel * dt)
            
            # check if temp_hitbox is outside the screen, and destroy
            # the entity if it is
            if not self._screen_rect.colliderect(temp_hitbox.rectangle):
                self.entity_manager.add(e, Destroyed())
                # remove the entity from its groups
                for cgroup in hitbox.collide_groups:
                    self._groups_dict[cgroup].remove(e)
                continue
            
            # check for collision
            for cgroup in hitbox.collide_groups:
                for o in self._groups_dict[cgroup]:
                    # check if entity is still available
                    if not self.entity_manager.hasEntity(o):
                        # remove it from the groups
                        for key in self._groups_dict.keys():
                            if o in self._groups_dict[key]:
                                self._groups_dict[key].remove(o)
                        continue
                    
                    if o == e:
                        continue
                    hitbox2 = self.entity_manager.get(o, Hitbox)
                    
                    # if both entities have a Type component, and if
                    # the types are the same, do not collide
                    # Also, Ennemies/Players shouldn't collide
                    a = self.entity_manager.get(e, Type)
                    b = self.entity_manager.get(o, Type)
                    if a and b and (a.type == b.type or \
                        (a.type == EntityType.Ennemy and b.type == EntityType.Player) or \
                            (a.type == EntityType.Player and b.type == EntityType.Ennemy)):
                        continue
                    
                    # if the player collides with an active door, attach the callback of the door
                    # to the player and quit the function, because we will change room
                    c = self.entity_manager.get(o, Door)
                    if a and b and c and a.type == EntityType.Player and c.active and temp_hitbox.rectangle.colliderect(hitbox2.rectangle):
                        self.entity_manager.add(e, c.callback)
                        return
                    
                    # if there is a collision
                    if temp_hitbox.rectangle.colliderect(hitbox2.rectangle):
                        colliding_with = o
                        break
            
            if colliding_with is None:
                position.move(movement.vel * dt)
                # regenerate the collide_groups list
                for cgroup in hitbox.collide_groups:
                    self._groups_dict[cgroup].remove(e)
                hitbox.collide_groups = None
            else:
                # add a contact component to notify the contact system
                self.entity_manager.add(e, Contact(colliding_with))