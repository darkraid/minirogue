import pygame
from .. import ecs
from ..components.renderable import Renderable
from ..components.position import Position
from ..components.layer import Layer


# the rendering system is doing a Z sorting based on the
# Layer component which should be attached to each entity
# to render, in order to be able to choose in which order
# the entities are drawn

class RenderingSystem(ecs.System):
    def __init__(self, entity_manager, screen, *args):
        super().__init__(entity_manager, Renderable, Position, Layer)
        self.screen = screen

    def update(self, dt):
        self.screen.fill((0, 0, 0))

        layers = {}
        for e, (r, p, l) in self:
            # update sprite
            r.update -= dt
            if r.update <= 0.0:
                r.current = (r.current + 1) % len(r.sprites)
                r.update = r.max_update

            # get layer of the sprite
            if l.z not in layers:
                layers[l.z] = []
            layers[l.z].append((r.sprite, p.position))
        
        # sort the layer regarding the Z component of the layer
        for sub in map(lambda x: x[1], sorted(layers.items())):
            for (sprite, position) in sub:
                self.screen.blit(sprite, position)