from .. import ecs
from ..components.projectile import Projectile
from ..components.movement import Movement
from ..components.renderable import Renderable
from ..components.layer import Layer
from ..components.hitbox import Hitbox
from ..components.position import Position
from ..components.weapon import Weapon, WeaponsHolder, WeaponType
from ..components.types import Type, EntityType
from ..components.damage import Damage
from ..constants import *


class WeaponSystem(ecs.System):
    def __init__(self, entity_manager, *args):
        super().__init__(entity_manager, WeaponsHolder, Projectile)

    def update(self, dt):
        to_remove = []
        to_generate = []

        # update cooldowns
        for entity, (wholder, projectile) in self:
            # remove the projectile
            to_remove.append(entity)

            for weapon in wholder.weapons:
                weapon.cooldown -= dt

                # only launch the projectile/attack if the weapon is active,
                # and if the cooldown is <= 0.0
                if weapon.active:
                    if weapon.cooldown > 0.0:
                        continue

                    # handle ranged weapons separatly from the contact weapons
                    if weapon.ranged:
                        projectile = self.entity_manager.get(wholder.owner, Projectile)
                        if projectile is None:
                            continue

                        o = ecs.Entity()
                        # move a little bit the projectile to avoid putting it immediatly on its summoner
                        # and thus applying damages to them
                        x = projectile.origin.x + projectile.vec_dir.x * projectile.origin_hitbox.w * 1.5
                        y = projectile.origin.y + projectile.vec_dir.y * projectile.origin_hitbox.h * 1.5

                        if weapon.type == WeaponType.Fireball:
                            to_generate.append({
                                "position": Position(x, y),
                                "movement": Movement(projectile.vec_dir * AMMO_SPEED_FIREBALL),
                                "renderable": Renderable("game/assets/fireball.png"),
                                "layer": Layer(z=1),
                                "hitbox": Hitbox((32, 32)),
                                "entity": o,
                                "type": Type(EntityType.Ammo),
                                "damage": Damage(DMG_FIREBALL, wholder.owner)
                            })
                        elif weapon.type == WeaponType.Plasma:
                            to_generate.append({
                                "position": Position(x, y),
                                "movement": Movement(projectile.vec_dir * AMMO_SPEED_PLASMA),
                                "renderable": Renderable("game/assets/plasma.png"),
                                "layer": Layer(z=1),
                                "hitbox": Hitbox((32, 32)),
                                "entity": o,
                                "type": Type(EntityType.Ammo),
                                "damage": Damage(DMG_PLASMA, wholder.owner)
                            })
                    else:
                        to_generate.append({
                            "position": Position(x, y),
                            "movement": Movement(Vec2D(0, 0)),
                            "renderable": None,
                            "layer": None,
                            "hitbox": Hitbox((24, 24)),
                            "entity": o,
                            "type": Type(EntityType.ContactDamage),
                            "damage": Damage(DMG_SWORD, wholder.owner)
                        })
                    
                    # reset cooldown
                    weapon.cooldown = weapon.max_cooldown
        
        # generate the ammo
        for data in to_generate:
            pos = data["position"]
            pos.x -= data["hitbox"].rectangle.w // 2
            pos.y -= data["hitbox"].rectangle.h // 2
            self.entity_manager.add(data["entity"], pos)
            self.entity_manager.add(data["entity"], data["movement"])
            if data["renderable"]:
                self.entity_manager.add(data["entity"], data["renderable"])
            if data["layer"]:
                self.entity_manager.add(data["entity"], data["layer"])
            self.entity_manager.add(data["entity"], data["hitbox"])
            self.entity_manager.add(data["entity"], data["type"])
            if data["damage"]:
                self.entity_manager.add(data["entity"], data["damage"])
        
        # remove components
        for entity in to_remove:
            self.entity_manager.removeComponent(entity, Projectile)