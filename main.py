import sys
import pygame
import game

def main(*args):
    pygame.init()
    pygame.key.set_repeat(100, 50)  # delay: milliseconds, repeat: milliseconds

    # window size in pixels
    screen = pygame.display.set_mode((game.WIDTH, game.HEIGHT))
    game.start(screen)

    pygame.quit()


if __name__ == '__main__':
    main(*sys.argv[1:])